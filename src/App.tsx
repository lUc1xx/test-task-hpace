import UserPage from 'pages/UserPage';
import UsersPage from 'pages/UsersPage';
import { Link, Route, Routes } from 'react-router-dom';
import RoutePaths from 'routes/RoutePaths';
import s from './App.module.scss';

function App() {
    return (
        <div className={s.wrapper}>
            <Routes>
                <Route path={RoutePaths.main.users} element={
                    <UsersPage />
                } />
                <Route path={RoutePaths.main.user} element={
                    <UserPage />
                } />
                <Route path="*" element={
                    <Link to={RoutePaths.main.users}>Пользователи</Link>
                } />
            </Routes>
        </div>
    );
}

export default App;
