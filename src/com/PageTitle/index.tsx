import s from './index.module.scss';

interface Props {
    value: string;
}

const PageTitle: React.FC<Props> = (props: Props): React.ReactElement => {
    const { value } = props;

    return (
        <h1 className={s.title}>{value}</h1>
    )
}

export default PageTitle;