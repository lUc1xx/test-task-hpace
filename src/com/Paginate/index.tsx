import s from './index.module.scss';
import { Link } from "react-router-dom";

interface Props {
    currentPage: number
    route: string
    handleChangeLimit: (value: number) => void
}

const Paginate: React.FC<Props> = (props: Props): React.ReactElement => {
    const { currentPage, route, handleChangeLimit } = props;

    const prevRoute = `${route}${currentPage - 1}`;
    const nextRoute = `${route}${currentPage + 1}`;

    const handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        handleChangeLimit(Number(event.target.value))
    }

    return (
        <div className={s.wrapper}>
            <div className={s.paginate}>
                <Link to={prevRoute}>&#8592;</Link>
                <div>{currentPage}</div>
                <Link to={nextRoute}>&#8594;</Link>
            </div>
            <div className={s.limit}>
                <select onChange={handleChange}>
                    <option value="5" selected>5</option>
                    <option value="10">10</option>
                    <option value="0">all</option>
                </select>
            </div>
        </div>
    )
}

export default Paginate;