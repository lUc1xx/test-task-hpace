import { Link } from 'react-router-dom';
import RoutePaths from 'routes/RoutePaths';
import { IUser } from 'types/UserTypes';
import s from './UserItemComponent.module.scss';

interface Props {
    data: IUser
    handleClickDelete: () => void
    editing: boolean
    changeEditing: (value: boolean) => void
    handleChangeName: (e: React.FormEvent<HTMLInputElement>) => void
    handleChangeAge: (e: React.FormEvent<HTMLInputElement>) => void
    editName: string
    editAge: number
    handleClickSave: () => void
}


const UserItemComponent: React.FC<Props> = (props: Props): React.ReactElement => {
    const { id, avatar, name, age, } = props.data;
    const {
        handleClickDelete, editing, changeEditing,
        handleChangeName, handleChangeAge, editName,
        editAge, handleClickSave
    } = props;

    return (
        <>
            <div className={s.wrapper}>
                <div className={s.info}>
                    <img src={avatar} alt={avatar} />
                    <Link to={`${RoutePaths.main.userLink}${id}`}>{name}</Link>
                    <p>Возраст: {age}</p>
                </div>
                <div className={s.actions}>
                    <button onClick={handleClickDelete}>Удалить</button>
                    <button onClick={() => changeEditing(true)}>Редактировать</button>
                </div>
            </div>

            {editing &&
                <div className={s.edit}>
                    <div className={s.edit__container}>
                        <p>Редактирование</p>
                        <input
                            type="text"
                            value={editName}
                            onChange={handleChangeName}
                        />
                        <input
                            type="text"
                            value={editAge}
                            onChange={handleChangeAge}
                        />
                        <button onClick={handleClickSave}>Сохранить</button>
                        <button onClick={() => changeEditing(false)}>Отмена</button>
                    </div>
                </div>
            }
        </>
    )

}

export default UserItemComponent;