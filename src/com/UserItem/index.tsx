import { useState } from "react";
import UsersStore from "store/UsersStore";
import { IUser } from "types/UserTypes";
import UserItemComponent from "./UserItemComponent";

interface Props {
    data: IUser;
}

const UserItem: React.FC<Props> = (props: Props): React.ReactElement => {
    const { data } = props;
    const [editing, setEditing] = useState<boolean>(false);
    const [editName, setName] = useState<string>(data.name);
    const [editAge, setAge] = useState<number>(data.age)

    const handleClickDelete = () => {
        UsersStore.deleteUser(data.id)
    }

    const changeEditing = (value: boolean) => {
        setEditing(value)
    }

    const handleChangeName = (e: React.FormEvent<HTMLInputElement>) => {
        let target = e.target as HTMLInputElement;
        setName(target.value)
    }

    const handleChangeAge = (e: React.FormEvent<HTMLInputElement>) => {
        let target = e.target as HTMLInputElement;
        setAge(Number(target.value))
    }

    const handleClickSave = () => {
        UsersStore.editUserInfo(data.id, editName, editAge)
        setEditing(false)
    }

    return (
        <UserItemComponent
            data={data}
            handleClickDelete={handleClickDelete}
            editing={editing}
            changeEditing={changeEditing}
            handleClickSave={handleClickSave}
            handleChangeName={handleChangeName}
            handleChangeAge={handleChangeAge}
            editName={editName}
            editAge={editAge}
        />
    )
}

export default UserItem;