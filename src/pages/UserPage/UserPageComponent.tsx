import PageTitle from 'com/PageTitle';
import { Link } from 'react-router-dom';
import RoutePaths from 'routes/RoutePaths';
import { IUser } from 'types/UserTypes';
import s from './UserPageComponent.module.scss';

interface Props {
    user: IUser | null
}


const UserPageComponent: React.FC<Props> = (props: Props): React.ReactElement | null => {
    const { user } = props;

    if (!user) {
        return null;
    }

    return (
        <div className={s.wrapper}>

            <Link to={RoutePaths.main.users}>Вернуться назад</Link>

            <PageTitle
                value={`Пользователь: ${user.id}`}
            />

            <p className={s.info}>
                Информация: 
                <br/>
                <br/>

                ID: {user.id}
                <br/>
                Имя: {user.name}
                <br/>
                Возраст: {user.age}
                <br/>
                Компания: {user.company.name}
                <br/>
            </p>

        </div>
    )

}

export default UserPageComponent;