import { useEffect } from "react";
import { useParams } from "react-router-dom";
import { observer } from "mobx-react-lite";
import UsersStore from "store/UsersStore";
import UserPageComponent from "./UserPageComponent";

const UserPage: React.FC = (): React.ReactElement => { 
    const { id } = useParams();

    useEffect(() => {
        UsersStore.getUser(Number(id));
    }, [id])

    return (
        <UserPageComponent
            user={UsersStore.user}
        />
    )
    
}

export default observer(UserPage);