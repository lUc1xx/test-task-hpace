import PageTitle from 'com/PageTitle';
import Paginate from 'com/Paginate';
import UserItem from 'com/UserItem';
import RoutePaths from 'routes/RoutePaths';
import { IUser, Order } from 'types/UserTypes';
import s from './UsersPageComponent.module.scss';

interface Props {
    list: IUser[]
    currentPage: number
    handleChangeLimit: (value: number) => void
    handleChangeSortOrder: (event: React.ChangeEvent<HTMLSelectElement>) => void
}


const UsersPageComponent: React.FC<Props> = (props: Props): React.ReactElement => {
    const { list, currentPage, handleChangeLimit, handleChangeSortOrder } = props;

    return (
        <div className={s.wrapper}>
            <PageTitle
                value='Список пользователей'
            />

            <div className={s.sort}>
                <p>Сортировка по возрасту:</p>
                <select onChange={handleChangeSortOrder}>
                    <option value={Order.ASC}>по возрастанию</option>
                    <option value={Order.DESC}>по убыванию</option>
                </select>
            </div>

            <div className={s.list}>
                {list.length > 1
                    ?
                    list.map((user: IUser) => {
                        return (
                            <UserItem data={user} key={user.id} />
                        )
                    })
                    :
                    <div>Список пуст</div>
                }
            </div>

            <Paginate
                currentPage={currentPage}
                route={RoutePaths.main.usersLink}
                handleChangeLimit={handleChangeLimit}
            />
        </div>
    )

}

export default UsersPageComponent;