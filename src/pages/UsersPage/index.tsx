import { observer } from "mobx-react-lite";
import { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import RoutePaths from "routes/RoutePaths";
import UsersStore from "store/UsersStore";
import { Order } from "types/UserTypes";
import UsersPageComponent from './UsersPageComponent';

const DEFAULT_PAGE = 1;

const UsersPage: React.FC = (): React.ReactElement => {
    const { page } = useParams();
    const [limit, setLimit] = useState<number>(5);
    const [sortOrder, setSortOrder] = useState<Order>(Order.ASC);
    const navigate = useNavigate();

    useEffect(() => {
        if (!Number(page)) {
            navigate(`${RoutePaths.main.usersLink}${DEFAULT_PAGE}`)
        } else {
            UsersStore.getUsers({
                page: Number(page),
                limit: limit,
                order: sortOrder
            });
        }
    }, [page, limit, sortOrder, navigate])

    const handleChangeLimit = (value: number) => {
        setLimit(value)
    }

    const handleChangeSortOrder = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const value = event.target.value as Order;
        setSortOrder(value);
    }

    return (
        <UsersPageComponent
            list={UsersStore.list}
            currentPage={Number(page)}
            handleChangeLimit={handleChangeLimit}
            handleChangeSortOrder={handleChangeSortOrder}
        />
    )
}

export default observer(UsersPage);