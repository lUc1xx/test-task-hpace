export const MainRoutes = {
    users: '/users/:page',
    usersLink: '/users/',
    user: '/user/:id',
    userLink: '/user/'
}