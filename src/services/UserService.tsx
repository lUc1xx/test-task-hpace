import axios from 'axios';
import { IGetUsersArg, IUser } from 'types/UserTypes';
import { BASE_URL } from './config';

const UsersEndpoint = "users"

const UserService = {
    getUsers: async function (arg: IGetUsersArg) {
        const { page, limit, order } = arg;
        const EndPoint = `${BASE_URL}${UsersEndpoint}`;
        let URL = `${EndPoint}?sortBy=age&page=${page}&order=${order}`;

        if (Boolean(limit)) URL += `&l=${limit}`;

        try {
            const response = await axios.get<IUser[]>(URL);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    getUser: async function (ID: number) {
        try {
            const response = await axios.get<IUser>(`${BASE_URL}${UsersEndpoint}/${ID}`);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    deleteUser: async function (ID: number) {
        try {
            const response = await axios.delete(`${BASE_URL}${UsersEndpoint}/${ID}`);
            return response.data;
        } catch (error) {
            throw error;
        }
    },
}

export default UserService;