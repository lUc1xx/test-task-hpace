import { makeAutoObservable } from "mobx";
import UserService from "services/UserService";
import { IGetUsersArg, IUser } from "types/UserTypes";

class UsersStore {

    list = [] as IUser[];
    user: IUser | null = null;

    constructor() {
        makeAutoObservable(this);
    }

    getUsers(arg: IGetUsersArg) {
        UserService.getUsers(arg)
            .then((users) => this.list = users)
            .catch(console.log)
    }

    deleteUser(ID: number) {
        UserService.deleteUser(ID)
            .then(() => {
                this.list = this.list.filter(item => item.id !== ID);
            })
            .catch(console.log)
    }

    editUserInfo(id: number, name: string, age: number) {
        this.list = this.list.map((user: IUser) => {
            if (user.id === id) {
                return { ...user, name: name, age: age }
            }
            return user;
        })
    }

    getUser(id: number) {
        UserService.getUser(id)
            .then((user) => this.user = user)
            .catch(console.log)
    }

}

export default new UsersStore();