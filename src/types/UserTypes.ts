
export interface IUser {
    id: number
    name: string
    age: number
    company: IUserCompany
    avatar: string
}

interface IUserCompany {
    name: string
    date: string
}

export interface IGetUsersArg {
    page: number
    limit: number
    order: Order
}

export enum Order {
    ASC = "asc",
    DESC = "desc"
}